// login mutations
export const USER_NOT_EXIST = 'USER_NOT_EXIST'
export const USER_NOT_FOUND = 'USER_NOT_FOUND'
export const PWD_NOT_MATCH = 'PWD_NOT_MATCH'
export const RESET_LOGIN = 'RESET_LOGIN'
export const TOGGLE_CHECK = 'TOGGLE_CHECK'

// register mutations
export const USER_NAME_PROMPT = 'USER_NAME_PROMPT'
export const USER_PASS_PROMPT = 'USER_PASS_PROMPT'
export const USER_PASS_AGAIN = 'USER_PASS_AGAIN'
export const TEL_NO_PROMPT = 'TEL_NO_PROMPT'
export const EMAIL_PROMPT = 'EMAIL_PROMPT'
export const RESET_REGISTER = 'RESET_REGISTER'
export const REGISTER_PROMPT = 'REGISTER_PROMPT'
export const AGREEMENT = 'AGREEMENT'

// home
export const CHANGE_LOGO_NAME = 'CHANGE_LOGO_NAME'

// login
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const USERINFO_SUCCESS = 'USERINFO_SUCCESS'
export const USERINFO_FAILURE = 'USERINFO_FAILURE'
export const LOGOUT_USER = 'LOGOUT_USER'
export const UPDATE_USER_SUCCESS = 'UPDATE_USER_SUCCESS'

// user
export const GET_USERS_SUCCESS = 'GET_USERS_SUCCESS'
export const SET_PAGINATION_INFO = 'SET_PAGINATION_INFO'
export const CHANGE_FIELD_VAL = 'CHANGE_FIELD_VAL'
export const TOGGLE_DIALOG = 'TOGGLE_DIALOG'
export const IS_EDIT_OR_ADD = 'IS_EDIT_OR_ADD'


export const SHOW_MSG = 'SHOW_MSG'
export const HIDE_MSG = 'HIDE_MSG'

// common