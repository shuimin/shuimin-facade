/**
 * Created by kevin on 17/4/6.
 */
/**
 * Created by SQB on 2015/9/7.
 */

module.exports =function(io,redis,mysql,moment,reward,Model){

    //here 客户端登录验证
    console.log('no validating');

    //聊天奖励金币时间
    var rewardDT = {
        rewardTime:"",
        userid:628,
        gold:2,
        status:'initing'
    }

    //第一次聊天奖励时间
    var rewardTime = moment().valueOf();
    console.log(moment(rewardTime).format('YYYY-MM-DD hh:mm:ss'),'-- 第一次聊天奖励金币时间');

    //功能函数
    //生成几分钟以内的随机数
    var generateMinute = function(t){
        return parseInt(t*60000*Math.random());
    }

    var generateReward = function(){
        var startTime = moment().valueOf();
        var getRandomTime = generateMinute(15);
        var rewardTime = startTime + getRandomTime;
        //修改奖励状态
        rewardDT.status = "generated";
        console.log('下次聊天奖励金币时间：',moment(rewardTime).format('YYYY-MM-DD hh:mm:ss'),'--奖励状态:',rewardDT.status);
        //修改下次奖励时间
        rewardDT.rewardTime = rewardTime;
    }
    //首次执行聊天奖励
    generateReward();

    //生成金币雨消息
    var getJinbiyuMsg = function(i){
        if(i==1){
            return '淅淅沥沥一阵小雨飘过，聊天室在线用户每人获得金币+1奖励';
        }else if(i==2){
            return '不经意间一场雷阵雨已经过去，聊天室在线用户每人获得金币+2奖励';
        }else if(i==3){
            return '让暴风雨来的更猛烈些吧，聊天室在线用户每人获得金币+3奖励';
        }
    };

    //金币雨
    function jinbiyu(){
        var nowTime = moment().format('HH');
        if(nowTime <= 20 && nowTime >= 8) {
            function containsID(arr1,comp){
                for(var i=0;i<arr1.length;i++){
                    if(arr1[i].userid == comp){
                        console.log('yes');
                        return true;
                    }
                }
                return false;
            }
            var rmDup = function (arr) {
                var tmparr = [];
                for (var i = 0; i < arr.length; i++) {
                    if (!containsID(tmparr, arr[i].userid)) {
                        tmparr.push(arr[i]);
                    }
                }
                return tmparr;
            };
            users = rmDup(users);

            var getRanGold = parseInt(3*Math.random())+1;
            for(var i=0;i<users.length;i++){
                var dt ={
                    userid:users[i].userid,
                    gold:getRanGold
                };
                reward.emit('addGold',dt);
            }
            var tmpdt = {
                time:moment().valueOf(),
                type:4,
                notice:getJinbiyuMsg(getRanGold),
                gold:getRanGold
            }
            io.emit('c2p',tmpdt);
        }

        var getRanTime = generateMinute(90);
        setTimeout(jinbiyu,getRanTime);
        console.log('金币雨将在'+moment(moment().valueOf()+getRanTime).fromNow()+'时间后开始'+'每人奖励'+getRanGold+'个金币！');
    }

    //第一次生成金币雨
    var firstGTime = generateMinute(90);
    console.log('金币雨将在'+moment(moment().valueOf()+firstGTime).fromNow()+'时间后开始');
    //在firstGtime时间后开始第一次金币雨，然后再间隔多长时间后调用自己
    setTimeout(jinbiyu,firstGTime);


    function noticeAD(){
        var subDT = {
            userid:1,
            nickname:"杰米诺",
            thum:'http://www.geminno.cn/files/user/2015/10-12/171044464b75564591.png',
            time:moment().valueOf(),
            type:4,
            notice:'<p><a style="font-weight:bold;color:red;line-height:20px;" href="http://www.geminno.cn/_pages/static/match1510.html" target="_blank"><div style="font-size:14px;">为学弟学妹们的课程设计出题！提交创意项目，合格就送100金币！</div><div style="font-size:24px;line-height:32px">课设创意大赛启动啦！点我点我！</div><div style="font-size:14px;">走心！用30分钟拿金币，60分钟争奖品！</div></a></p>'
        };
        io.emit('c2p',subDT);
    }
    //每隔10m发送一次广告
    setInterval(noticeAD,600000);


    //socket.io部分
    io.set('heartbeat interval', 5000);
    io.set('heartbeat timeout', 60000);

    var messageBox = {};
    var users = [];
    var reconnect_count = 0;

    console.log('start connection...');
    io.on('connection',function(socket){
        //获取socket的id
        var socketID = socket.id;
        //给每个socketID一个数组
        messageBox[socketID] = [];
        //存储当前socket的userInfo
        var userInfo = {
            userid:"",
            nickname:"",
            thum:""
        };

        //init(),一建立连接的时候传入用户信息，包括重新登录的时候，这里不用客户端的connect，兼容到火狐高版本问题，socket没有监听connect事件
        socket.on('onInit',function(data){
            //兼容google问题，导航栏输入URL就自动请求
            console.log('onInit...',data.room);
            if(data.userid == undefined){
                console.log('没有用户id，没有用户信息！');
                return;
            }
            userInfo.userid = data.userid;
            userInfo.nickname = data.nickname;
            userInfo.thum = data.thum;
            //触发客户端onInit事件
            io.in(socketID).emit('onInit',data);

            //查看redis中是否存在'userid*'
            redis.store.exists('userid'+userInfo.userid,function(err,data){
                console.log(data,'是否存在','userid'+userInfo.userid);
                if(err){
                    console.log('if exists error',err);
                }else{
                    if(data == 1){
                        console.log('online users: userid'+userInfo.userid+'exists');
                    }else if(data == 0){
                        console.log('online users: userid'+userInfo.userid+'not exists');
                        users.push(userInfo);
                        var doc = {
                            userid:userInfo.userid,
                            nickname:userInfo.nickname,
                            thum:userInfo.thum,
                            loginTime:moment().valueOf(),
                            logoutTime:moment().valueOf()
                        };

                        //将信息存入mongodb
                        new Model.LoginModel(doc).save(function(err){
                            if(err){
                                console.log(err);
                            }else{
                                console.log('用户登录信息存储 OK！ID：',userInfo.userid);
                            }
                        });

                        //记录本次登录时间到redis，下次登录之前可以查出来，然后再覆盖，可以算出上次登录时间。
                        redis.pub.hmset('user:'+userInfo.userid,doc);
                        redis.pub.hmset('user:'+userInfo.userid,userInfo);

                    }

                    //socket信息存在redis中
                    redis.store.sadd("userid"+userInfo.userid,socketID);
                    console.log('用户socketid存入redis,socketid为',socketID);
                }
            })
        });


        //如果是服务器宕机，客户端重新连接reconnect
        socket.on('reInit',function(data) {
            console.log('reInit...', data.room);
            reconnect_count++;
            //第一次重新链接的客户端清空所有在线用户信息
            if (reconnect_count == 1) {
                users = [];
                console.log('第一次重新连接，清空所有在线用户......');
            }
            //兼容google的傻逼问题，导航栏输入就自动请求
            if (data.userid == undefined) {
                console.log('没有用户id,没有用户信息');
                return;
            }
            userInfo.userid = data.userid;
            userInfo.nickname = data.nickname;
            userInfo.thum = data.thum;
            //触发客户端onInit事件
            io.in(socketID).emit('onInit', data);

            //判断重新连接用户有没有在线reids
            redis.store.exists('userid_reconnect'+userInfo.userid,function(err,data){
                console.log(data,'是否存在','userid'+userInfo.userid);
                if(err){
                    console.log('if reconennt exits',err);
                }else{
                    if(data == 1){
                        console.log('reconnect:online users :userid'+userInfo.userid+'exists');
                    }else if(data == 0){
                        console.log('reconnect:online users :userid'+userInfo.userid+'not exists');
                        redis.store.del('userid'+userInfo.userid);
                        redis.store.sadd('userid'+userInfo.userid,'reconnected');
                        users.push(userInfo);

                        var doc = {
                            userid: userInfo.userid,
                            nickname: userInfo.nickname,
                            thum:userInfo.thum,
                            loginTime:moment().valueOf(),
                            logoutTime:moment().valueOf()
                        };
                        new Model.LoginModel(doc).save(function(err){
                            if(err){
                                console.log(err);
                            }else{
                                console.log('reconnect:用户登录信息存储 OK！id：',userInfo.userid);
                            }
                        });

                        //存入重新连接状态
                        doc.status = 'reconnected';
                        redis.pub.hmset('user:'+userInfo.userid,doc);
                    }

                    //socket信息存在redis中
                    redis.store.sadd('userid'+userInfo.userid,socketID);
                    console.log('reconnect:用户socketID存入redis，socketid为',socketID);
                }
            });

            //设置重新连接延时参数
            redis.store.setex('userid_reconnect'+userInfo.userid,10,'reconnect');
        });

        //心跳检测，默认客户端每5秒跳动一次
        socket.conn.on('heartbeat',function(){
            var client_ip_address = socket.request.connection.remoteAddress;
            //console.log('heartbeat',socket.id,client_ip_address,moment().format('YYYY-MM-DD h:mm:ss'));
            //服务器端断开，客户端会重新链接
            //socket.conn.close();

            //服务器端删除socket
            //socket.disconnect(true);

            //关闭io服务，不能链接
            //io.close();

            //每次心跳，将用户id对应的key的超时时间设置为60秒
            if(userInfo.userid){
                redis.store.expire('userid'+userInfo.userid,60);
            }

            //获取所有存储的用户id对应的用户信息
            //redis.pub.hgetall('user:'+userInfo.userid,function(err,data){
            //    if(err){
            //        console.log(err);
            //    }
            //    console.log(data);
            //});

            var onlineUserInfo = {
                //users:users,
                count:users.length
            };

            //获取所有在线人员信息
            socket.emit('onlineUsers',onlineUserInfo);
            //console.log('heart break);
        });

        socket.on('ranFriend',function(){
            function containsID(arr1,comp){
                for(var i=0;i<arr1.length;i++){
                    if(arr1[i].userid == comp){
                        return true;
                    }
                }
                return false;
            }

            var rmDup = function(arr){
                var tmparr = [];
                for(var i=0;i<arr.length;i++){
                    if(!containsID(tmparr,arr[i].userid)){
                        tmparr.push(arr[i]);
                    }
                }
                return tmparr;
            }

            users = rmDup(users);

            var ranFriendInfo = {
                users:users,
                count:users.length
            };
            if(users.length <= 6){
                socket.emit('ranFriend',ranFriendInfo);
            }else{
                function getRanFriend(arr,num){
                    var return_arr = [];
                    var index = Math.ceil(Math.random()*(users.length))-1;
                    if(index+num <= arr.length){
                        for(var i=index;i<index+num;i++){
                            return_arr.push(arr[i]);
                        }
                    }else if(index+num>arr.length){
                        for(var i=index;i<arr.length;i++){
                            return_arr.push(arr[i]);
                        }
                        for(var i=0;i<index+num-arr.length;i++){
                            return_arr.push(arr[i]);
                        }
                    }
                    return return_arr;
                }

                ranFriendInfo.users = getRanFriend(users,6);
                socket.emit('ranFriend',ranFriendInfo);
            }
            console.log('用户ID：'+userInfo.userid,'用户名：'+userInfo.nickname,'执行随机伙伴函数','总伙伴数为：'+users.length)
        });

        //监听聊天信息
        socket.on('p2c',function(data){
            //根据用户id查询用户信息
            //灌水
            if(data.type == 1){
                //对本聊天室内所有人发送信息
                data.time = moment().valueOf();

                var edge = 10;
                if(messageBox[socketID].length<edge){
                    messageBox[socketID].push(data);
                }else if(messageBox[socketID].length>=edge){
                    messageBox[socketID].splice(0,1);
                    messageBox[socketID].push(data);

                    //if(messageBox[socketID][messageBox[socketID].length>=edge]){
                    if(messageBox[socketID][messageBox[socketID].length-1].time-messageBox[socketID][0].time<=60000){
                        console.log('有屌丝在刷金币！！！');
                        var gdt = {
                            userid:data.userid,
                            nickname:data.nickname,
                            thum:data.thum,
                            type:4,
                            notice:'非手工刷金币，被无情的T出了聊天室！',
                            time:moment().valueOf()
                        };
                        io.emit('c2p',gdt);
                        delete messageBox[socketID];
                        socket.disconnect(true);
                        return;
                    }
                }
                io.emit('p2c',data);

                //随机奖励用户
                if(rewardDT.status == 'generated'){
                    var rewardTime = rewardDT.rewardTime;
                    //当前获取信息时间
                    var getMessageTime = moment().valueOf();
                    var hourTime = moment().format('HH');
                    if(getMessageTime>rewardTime&&hourTime<=20&&hourTime>=8){
                        var dt = {
                            rewardTime:rewardTime,
                            userid:userInfo.userid,
                            gold:parseInt(10*Math.random())
                        };
                        console.log('取得message时间：',getMessageTime,'奖励时间：',rewardTime);
                        console.log('开始奖励：'+userInfo.userid,'用户：'+userInfo.nickname,'金币数：'+dt.gold);
                        reward.emit('addGold',dt);
                        var tmpdata = data;
                        tmpdata.type = 4;
                        tmpdata.time = moment().valueOf();
                        tmpdata.gold = dt.gold;
                        tmpdata.thum = userInfo.thum;
                        tmpdata.notice = '幸运的发言，被系统赠送'+dt.gold+'个金币！';
                        io.emit('c2p',tmpdata);
                        rewardDT.status = 'rewarded';
                        generateReward();
                    }
                }
            }
            //课程提问
            else if(data.type == 2){
                if(data.to.userid){
                    console.log('用户：'+data.userid,'用户名：'+data.nickname+"对->",data.to.userid,' 用户：'+data.to.nickname+'私语：',data.content);
                    redis.store.smembers("userid"+data.to.userid,function(err,res){
                        if(err){
                            console.log('私信错误：'+err);
                        }
                        else{
                            //console.log('here@',res);
                            res.forEach(function(i){
                                console.log('对客户端socketid',i,'私语',data);
                                io.to(i).emit('p2c',data);
                            });
                        }
                    });
                }
            }
            //项目提问
            else if(data.type == 3){
                //对所有人发送提问问题
                data.nickname = userInfo.nickname;
                data.time = moment().valueOf();
                io.emit('p2c',data);
                console.log('系统对所有人发送问题：',data);
            }else{
                console.log('消息发送不正确！');
                return;
            }
        });

        socket.on('p2p',function(data){
            //私信悄悄话
            data.time = moment().valueOf();
            console.log("用户："+data.userid,'用户名：',data.nickname+'对->',data.to.userid,' 用户：'+data.to.nickname+"私语： ",data.content);
            if(data.to.userid){
                console.log('发送给私语对方消息： ');
                redis.store.smembers('userid'+data.to.userid,function(err,res){
                    if(err){
                        console.log('私信错误：'+err);
                    }else{
                        res.forEach(function(i){
                            console.log('对客户端socketid',i,'私语：',data);
                            io.to(i).emit('p2p',data);
                        });
                    }
                });
            }
            if(data.userid){
                console.log('发送给私语本人信息：');
                redis.store.smembers('userid'+data.userid,function(err,res){
                    if(err){
                        console.log('私信错误self:'+err)
                    }else{
                        res.forEach(function(i){
                            console.log('对客户端socketid',i,'私语：',data);
                            io.to(i).emit('p2p',data);
                        });
                    }
                });
            }
        });

        socket.on('disconnect',function(){
            console.log('用户：'+userInfo.userid,' 用户名：'+userInfo.nickname,'的'+socket.id,'disconnect');
            function removeUser(value){
                return (value.userid != userInfo.userid);
            }

            redis.store.scard('userid'+userInfo.userid,function(err,data){
                console.log('用户：'+userInfo.userid,' 用户名：'+userInfo.nickname,'的在线终端有'+data,'个');
                if(err){
                    console.log(err);
                }else if(data == 0){
                    console.log('如果是0个：');
                }
                else if(data == 1){
                    console.log('如果是1个：');
                    users = users.filter(removeUser);
                    redis.store.srem('userid'+userInfo.userid,socketID);

                    var dt = {
                        status:'offline'
                    };
                    redis.pub.hmset('user:'+userInfo.userid,dt);
                    console.log('no need take place !');
                }else if(data == 2){
                    console.log('如果是2个：');
                    redis.store.smembers('userid'+userInfo.userid,function(err,res){
                        if(err){
                            console.log('私信错误：'+err);
                        }else{
                            redis.store.srem('userid'+userInfo.userid,socketID);
                            res.forEach(function(i){
                                console.log('所有的res数据',res[i]);
                                if(i == 'reconnected'){
                                    console.log('其中有reconnected',res[i]);
                                    users = users.filter(removeUser);
                                    redis.store.del('userid'+userInfo.userid);

                                    var dt = {
                                        status:'offline'
                                    };
                                    redis.pub.hmset('user:'+userInfo.userid,dt);
                                }
                            });
                        }
                    });
                }else{
                    console.log('删除redis中'+'用户：'+userInfo.userid,' 用户名：'+userInfo.nickname,'的'+socketID);
                    redis.store.srem('userid'+userInfo.userid,socketID);
                }
            });
        });

        socket.on('firefox',function(data){
            console.log('welcome directive to firefox!',data);
            socket.join('firefox');
            //broadcast方法允许当前socket client 不在该分组内
            //socket.broadcast.to('chrome').emit('event_name',data);
            io.sockets.to('firefox').emit('ff_event',data);
        });

        socket.on('chrome',function(data){
            console.log('here!chrome!',data);
            socket.join('chrome');
            io.sockets.to('chrome').emit('event_name',data)
        });

        socket.on('p2p',function(){

        });

    });
};
