/**
 * Created by kevin on 17/4/5.
 */
var superagent = require('supertest')
var app = require('../facade')
var expect = require('chai').expect;

function request() {
    return superagent(app.listen())
}

describe('Routes', function() {
    describe('GET /', function() {
        it('should return 200', function(done) {
            request()
                .get('/')
                .expect(200, done)
        })
        it('should be json', function(done) {
            request()
                .get('/user/1')
                .expect(200)
                .end(function(err, res) {
                    if(err) {
                        return done(err)
                    }
                    var text = res.text;
                    var json = JSON.parse(text);
                    expect(json).to.have.property('status');
                    done();
                })
        })
    })
})