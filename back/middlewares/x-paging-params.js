/**
 * Created by kevin on 16/8/10.
 */
module.exports = pagingParams;

/**
 * Add X-Response-Time header field.
 *
 * @return {Function}
 * @api public
 */

function pagingParams() {
    return function *pagingParams(next){

        if(this.method == 'GET') {
            var obj = {};
            if(this.request && this.request.query && this.request.query.limit) {
                obj.limit = this.request.query.limit;
                if(this.request.query.offset) {
                    obj.offset = this.request.query.offset;
                }
            }else if(this.request && this.request.body && this.request.body.paging) {
                obj.limit = this.request.body.paging.limit;
                if(this.request.body.paging.offset) {
                    obj.offset = this.request.body.paging.offset;
                }
            }
            console.log(obj, 'this is obj')
            this.paging = obj;
        }
        yield *next;
    }
}