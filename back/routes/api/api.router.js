/**
 * Created by kevin on 16/8/10.
 */
var Router = require('koa-router');
var request = require('co-request')
var UserService = require('../../services/controllers/user.ctrl')
var RankService = require('../../services/controllers/rank.ctrl')
var ShopService = require('../../services/controllers/shop.ctrl')
var ApiService = require('../../services/controllers/apiproxy.ctrl')
var RBACService = require('../../services/controllers/rbac.ctrl')

function register(app) {
    var router = new Router({
        prefix: '/api'
    });
    router.post('/gettoken', function *() {
        console.log('getting token.....');
        ApiService.gettoken('localhost', uid, pwd)

        console.log(result)
        return this.body = ''
    })

    router.get('/users/', function *(next) {
        var opt = {};
        var sys = this.request.query['sys'];
        opt.url = this.request.query['url'];
        opt.auth = this.headers['auth'];


        var result = yield *UserService.getUser(sys, opt);

        console.log(result.statusCode, 'this is users opt');

        if(result.statusCode == 200) {
            var users = JSON.parse(result.body);
            return this.body = RES.SUCCESS({rows: users,count: users.length}, '返回'+sys+'用户成功', 200)
        }
        return this.body = RES.ERROR({}, "返回失败!", 500);

    }).get('/users/:user', function *(next) {
        var opt = {};
        var sys = this.request.query['sys'];
        opt.url = this.request.query['url'];
        opt.auth = this.headers['auth'];
        opt.user = this.params['user'];

        var result = yield *UserService.getOneUser(sys, opt);
        console.log(result.body, 'this is users opt' + opt.user);


        var user = JSON.parse(result.body);

        console.log(user, 'this is user - - - -')
        return this.body = RES.SUCCESS({row: user,count: user.length}, '返回'+sys+'用户成功', 200)
    }).get('/users/:userid/ranks/', function *(next) {
        var opt = {};
        var sys = this.request.query['sys'];
        opt.url = this.request.query['url'];
        opt.auth = this.headers['auth'];
        opt.userid = this.params['userid'];
        var result = yield * UserService.getRanks(sys, opt);
        console.log(result.body, 'this is user-ranks');
        if(result.statusCode == 200) {
            return this.body = RES.SUCCESS({rows: JSON.parse(result.body)}, '返回成功', 200)
        }else {
            return this.body = RES.ERROR({err: result.body}, '返回失败', 403)
        }
    }).post('/users', function *(next) {
        var opt = {};
        opt.auth = this.headers['auth'];
        opt.userinfo = this.request.body.dt;
        opt.url = this.request.body.url;

        sys = this.request.body.sys;
        console.log(opt.userinfo, 'this -s- -s -s- -s userinfo')
        var result = yield *UserService.postUser(sys, opt);

        console.log(result, 'this post result!')
        if(result.statusCode == 200) {
            return this.body = RES.SUCCESS({}, '返回成功', 200)
        }else {
            return this.body = RES.ERROR({err: result.body}, '返回失败', 200)
        }
    }).post('/users/:userid/ranks/:rankid', function *(next) {
        var opt = {};
        opt.auth = this.headers['auth'];
        opt.url = this.request.body.url;
        var sys = this.request.body.sys;
        opt.userid =  this.params['userid'];
        opt.rankid =  this.params['rankid'];

        console.log(opt.userinfo, 'this -s- -s -s- -s userinfo')
        var result = yield *UserService.postRank(sys, opt);

        console.log(result.body, 'this postrank result!')
        if(result.statusCode == 200 || result.statusCode == 204) {
            return this.body = RES.SUCCESS({}, '返回成功', 200)
        }else {
            return this.body = RES.ERROR({err: result.body}, '返回失败', 200)
        }
    }).put('/users/:user', function *(next) {
        // console.log(this.params['user'],this.request.body);
        var opt = {};
        opt.auth = this.headers['auth'];
        opt.uid = this.params['user'];
        opt.userinfo = this.request.body.dt;
        opt.url = this.request.body.url;

        var sys = this.request.body['sys'];

        console.log(opt, sys, 'this is usersusers opt')

        var result = yield *UserService.putUser(sys, opt);

        if(result.statusCode == 200 && result.body == this.params['user']) {
            return this.body = RES.SUCCESS({uid: this.params['user'] }, '返回uid成功', 200)
        }else {
            return this.body = RES.ERROR({uid: this.params['user'] }, '返回uid失败', 500)
        }
    }).delete('/users/:user', function *(next) {
        var opt = {};
        opt.auth = this.headers['auth'];
        opt.uid = this.params['user'];
        opt.url = this.request.body.url;
        var sys = this.request.body.sys;
        var result = yield *UserService.deleteUser(sys, opt);
        console.log(result.body, result.statusCode);
        if(result.statusCode == 201 && result.body == this.params['user']) {
            return this.body = RES.SUCCESS({uid: this.params['user'] }, '返回uid成功', 200)
        }else if(result.body) {
            return this.body = RES.ERROR({err: result.body }, '返回失败', 403)
        }
    }).delete('/users/:userid/ranks/:rankid', function *(next) {
        var opt = {};
        opt.auth = this.headers['auth'];
        opt.userid = this.params['userid'];
        opt.rankid = this.params['rankid'];
        opt.url = this.request.body.url;
        var sys = this.request.body.sys;
        var result = yield *UserService.deleteRank(sys, opt);
        console.log(result.body, result.statusCode, 'this is delete rankid');
        if(result.statusCode == 204) {
            return this.body = RES.SUCCESS({}, '返回uid成功', 204)
        }else {
            return this.body = RES.ERROR({err: result.body }, '返回失败', 403)
        }
    })

    router.get('/shops', function *(next) {
        var auth = this.headers['auth']
        var opt = {};
        var sys = this.request.query['sys'];
        opt.url = this.request.query['url'];
        opt.auth = this.headers['auth'];

        var result = yield *ShopService.getShops(sys, opt);

        console.log(opt, 'this is shops opt', result.body);

        var shops = JSON.parse(result.body);
        return this.body = RES.SUCCESS({rows: shops,count: shops.length}, '返回'+ sys+'shops成功', 200)
    });


    router.get('/ranks/', function *(next) {
        var opt = {};
        var sys = this.request.query['sys'];
        opt.url = this.request.query['url'];
        opt.auth = this.headers['auth'];


        var result = yield *RankService.getRanks(sys, opt);

        console.log(result.statusCode, 'this is users opt' + 'ranks');

        if(result.statusCode == 200) {
            var ranks = JSON.parse(result.body);
            return this.body = RES.SUCCESS({rows: ranks,count: ranks.length}, '返回'+sys+'权限成功', 200)
        }
        return this.body = RES.ERROR({}, "返回失败!", 500);

    });

    router.get('/rbac/users/:userid/roles/', function *(next) {
        var opt = {};
        var sys = this.request.query['sys'];
        opt.url = this.request.query['url'];
        opt.auth = this.headers['auth'];
        opt.userid = this.params['userid'];

        var result = yield *RBACService.getRoles(sys, opt);
        console.log(result.statusCode, result.body, 'this is rbac opt');

        if(result.statusCode == 200) {
            var roles = JSON.parse(result.body);
            return this.body = RES.SUCCESS({rows: roles,count: roles.length}, '返回'+sys+'权限成功', 200)
        }
        return this.body = RES.ERROR({}, "返回失败!", 500);
    })



    app.use(router.routes());
    app.use(router.allowedMethods());
}

module.exports = register;