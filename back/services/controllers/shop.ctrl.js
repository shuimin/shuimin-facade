/**
 * Created by kevin on 16/12/8.
 */
var db = require('../models');
var User = db.User;
var request = require('co-request')

var export_out = {
    getShops: function *(sys, opt) {
        var headers = {
            Authorization: opt.auth
        }

        var options = {
            url: opt.url + '/api/shops/',
            method: 'GET',
            headers: headers,
        }

        var result =  yield request(options);
        return result;
    }
}

module.exports = export_out