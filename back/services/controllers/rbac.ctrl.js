/**
 * Created by kevin on 16/12/8.
 */
var db = require('../models');
var request = require('co-request')
var base64 = require('../../util/base64')

var export_out = {
    getRoles: function *(sys, opt) {
        var headers = {
            Authorization: opt.auth
        }

        var options = {
            url: opt.url + '/api/rbac/users/' + opt.userid + '/roles/',
            method: 'GET',
            headers: headers
        }

        console.log(options.url);

        var result =  yield request(options);
        return result;
    },
}

module.exports = export_out