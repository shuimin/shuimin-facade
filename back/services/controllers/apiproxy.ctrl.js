/**
 * Created by kevin on 17/4/18.
 */
var request = require('co-request')

var export_out = {
    getToken: function *(url, uid, pwd) {
        var headers = {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
        var data = {
            username: uid,
            password: pwd
        }
        var options = {
            url: url + '/gettoken',
            method: 'POST',
            headers: headers,
            formData: data
        }
        var result =  yield request(options);
        return result;
    },
    loginCRM: function *(url, basicAuth) {

        var headers = {
            Authorization: basicAuth
        };

        var options = {
            url: url + '/api/shops',
            method: 'GET',
            headers: headers
        }
        var result = yield request(options)
        return result;
    },
    getUsers: function *(jwt, sessionId) {
        var headers = {
            // 'Content-Type': 'application/x-www-form-urlencoded',
            // 'Authorization': jwt,
            // 'Cookie': 'x-pond-sessionid=' + sessionId

            Authorization: jwt,
            Cookie: 'x-pond-sessionid=' + sessionId,
            // Host: 'localhost:5584',
            // Referer: 'http://localhost:5584/users'
        };

        console.log(jwt, sessionId);

        var options = {
            url: 'http://localhost:5584/api/users/',
            method: 'GET',
            headers: headers,
        };
        var result = yield request(options);
        return result;
    }
}

module.exports = export_out