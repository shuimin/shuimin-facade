/**
 * Created by kevin on 16/12/8.
 */
var db = require('../models');
var request = require('co-request')
var base64 = require('../../util/base64')
const uuidv4 = require('uuid/v4');

var export_out = {
    getUserByUid: function *(uid) {
       return yield db.User.find({ where: {
           uid: uid
       }})
    },
    createUser: function *(user) {
        user.uuid = uuidv4()
        return  yield db.User.create(user);
    },

    getUsersWithPaging: function *(dt) {
        return yield db.User.findAndCountAll({
            order: '`id` DESC',
            limit: parseInt(dt.limit),
            offset: (parseInt(dt.offset) - 1) * parseInt(dt.limit)
        })
    },

    updateUserByUid: function *(field) {
        return yield db.User.update(field, {
            where: {
                uid: field.uid
            }
        })
    },
    deleteUserByUid: function *(uid) {
        return yield db.User.destroy({
            where: {
                uid: uid
            }
        })
    },

    getOneUser: function *(sys, opt) {
        var headers = {
            Authorization: opt.auth
        }

        var options = {
            url: opt.url + '/api/users/' + opt.user,
            method: 'GET',
            headers: headers
        }

        var result =  yield request(options);
        return result;
    },

    getRanks: function *(sys, opt) {
        var headers = {
            Authorization: opt.auth
        }

        var options = {
            url: opt.url + '/api/users/' + opt.userid + '/ranks/',
            method: 'GET',
            headers: headers
        }
        var result = yield request(options);
        return result;
    },
    postUser: function *(sys, opt) {
        var headers = {
            Authorization: opt.auth
        }
        console.log(opt.userinfo, 'this is userinfo!')
        var options = {
            url: opt.url + '/api/users/',
            method: 'POST',
            headers: headers,
            form: opt.userinfo
        }
        var result =  yield request(options);
        return result;
    },
    postRank: function *(sys, opt) {
        var headers = {
            Authorization: opt.auth
        }
        var options = {
            url: opt.url + '/api/users/' + opt.userid + '/ranks/' + opt.rankid,
            method: 'POST',
            headers: headers
        }
        var result =  yield request(options);
        return result;
    },
    putUser: function *(sys, opt) {
        var headers = {
            Authorization: opt.auth
        }
        // if(opt.userinfo&& (opt.userinfo.sex || opt.userinfo.sex == '0')) {
        //     console.log('iam putting ', opt.userinfo)
        //     opt.userinfo.sex = parseInt(opt.userinfo.sex);
        //     console.log('iam puttinged ', opt.userinfo)
        // }
        var options = {
            url: opt.url + '/api/users/' + opt.uid,
            method: 'PUT',
            headers: headers,
            form: opt.userinfo
        }
        var result =  yield request(options);
        return result;
    },
    deleteUser: function *(sys, opt) {
        var headers = {
            Authorization: opt.auth
        }
        var options = {
            url: opt.url + '/api/users/' + opt.uid,
            method: 'DELETE',
            headers: headers
        }
        var result =  yield request(options);
        return result;
    },
    deleteRank: function *(sys, opt) {
        var headers = {
            Authorization: opt.auth
        }
        var options = {
            url: opt.url + '/api/users/' + opt.userid + '/ranks/' + opt.rankid,
            method: 'DELETE',
            headers: headers
        }
        var result =  yield request(options);
        return result;
    },
}

module.exports = export_out