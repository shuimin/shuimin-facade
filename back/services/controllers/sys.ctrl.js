/**
 * Created by kevin on 17/4/12.
 */
var db = require('../models');
var Sys = db.Sys;

var export_out = {
    findSwitchOn: function *() {
        var sys = yield Sys.findAndCountAll({
            where: {
                status: 1
            }
        });
        return sys;
    },
    findAndCountAll: function *(paging,query) {
        delete query.limit
        delete query.offset
        var where;
        var all = {};
        var obj = {};
        for(var i in query) {
            obj[i] = {
                $like: '%' + query[i] +'%'
            }
        }
        if(_.isEmpty(obj)) {
            where = {}
        }else {
            where = {
                $or: obj
            }
        }
        if(_.isEmpty(paging)) {
            all = {
                where: where
            }
        }else if(paging.limit && paging.offset) {
            all = {
                where: where,
                limit: Number(paging.limit),
                offset: Number(paging.offset)
            }
        }
        var all_count = yield Sys.findAndCountAll(all);
        return all_count;
    },
    create: function *(dt) {
        var created = yield Sys.create(dt);
        console.log('this is created', created)
        return created
    },
    deleteById: function *(dt) {
        var deleted = yield Sys.destroy({
            where: {
                id: dt.id
            }
        });
        console.log('this is deleted', deleted)
        return deleted
    },
    updateById: function *(dt) {
        delete dt.updated_at;
        var updated = yield Sys.update(dt, {
            where: {
                id: dt.id
            }
        });
        console.log('this is updated', updated)
        return updated;
    }
}

module.exports = export_out