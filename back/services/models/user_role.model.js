/**
 * Created by kevin on 17/4/11.
 */
module.exports = function(sequelize, Sequelize) {
    var UserRole = sequelize.define('UserRole',
        {
            uid: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            role_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                defaultValue: 0
            },
            created_at: {
                type:Sequelize.DATE
            },
            updated_at: {
                type:Sequelize.DATE
            },
            deleted_at: {
                type:Sequelize.DATE
            }
        },
        {
            'freezeTableName': true,
            'tableName': 'r_user_role',
            'timestamps': true,
            // 'createdAt': 'create_time',
            // 'updatedAt': false,
            // 将deletedAt字段改名
            // 同时需要设置paranoid为true（此种模式下，删除数据时不会进行物理删除，而是设置deletedAt为当前时间
            // 'deletedAt': 'dtime',
            // 'paranoid': true,
            defaultScope: {
                where: {
                    // username: 'dan'
                },
                limit: 12
            },
            classMethods: {
                associate: function(models) {
                    // UserRole.hasMany(models.User
                    //     , {foreignKey: 'user_id'}
                    // )
                    models.User.belongsToMany(models.Role, {'through': UserRole, foreignKey: 'uid', unique: false})
                    models.Role.belongsToMany(models.User, {'through': UserRole, foreignKey: 'role_id', unique: false})
                }
            }
        });
    // model.removeAttribute('id');
    // model.sync({force:true});
    // User.create({
    //     uuid: '005f9c2db58b4d0186235c0ee84d9a93',
    //     uid: 'HHBK25',
    //     name: 'mmmm',
    //     email: 'eeeeemail',
    //     tel: '15150121121',
    //     sex: 0,
    //     status: 1,
    //     password: '123456',
    //     shop_id: 'ggggg',
    //     avatar_id: 'bbbbbbbbbbasdjfklasjflaksjdflkasjflaksjflkasjflkasjflkasjdflkasflaskfdjl',
    //     remark: 'asdkjflaksjflaksjdflkasjflkasjflasfahjshfdjkashfoiweroiqwuroiqwuroiqwjrlqwjerlkqwejrklqwj',
    //     rank: '123',
    // })

    UserRole.sync();
    return UserRole;
}