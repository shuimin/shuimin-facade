/**
 * Created by kevin on 16/12/12.
 */
module.exports = function(sequelize, Sequelize) {
    var User = sequelize.define('User',
        {
            uuid: {
                type: Sequelize.STRING,
                allowNull: false,
                unique: true
            },
            uid: {
                type: Sequelize.STRING,
                allowNull: false,
                unique: true
            },
            name: {
                type: Sequelize.STRING,
            },
            is_admin: {
                type: Sequelize.INTEGER,
                defaultValue: 0
            },
            email: {
                type: Sequelize.STRING
            },
            tel: {
                type: Sequelize.STRING,
            },
            sex: {
                type: Sequelize.INTEGER,
                defaultValue: 0
            },
            status: {
                type: Sequelize.INTEGER,
                defaultValue: 0
            },
            corp: {
                type: Sequelize.STRING
            },
            password: {
                type: Sequelize.STRING
            },
            shop_id: {
                type: Sequelize.STRING
            },
            avatar_id: {
                type: Sequelize.STRING
            },
            remark: {
                type: Sequelize.STRING
            },
            rank: {
                type: Sequelize.STRING
            },
            created_at: {
                type:Sequelize.DATE
            },
            updated_at: {
                type:Sequelize.DATE
            },
            deleted_at: {
                type:Sequelize.DATE
            }
        },
        {
            'freezeTableName': true,
            'tableName': 't_user',
            'timestamps': true,
            // 'createdAt': 'create_time',
            // 'updatedAt': false,
            // 将deletedAt字段改名
            // 同时需要设置paranoid为true（此种模式下，删除数据时不会进行物理删除，而是设置deletedAt为当前时间
            // 'deletedAt': 'dtime',
            'paranoid': true,
            defaultScope: {
                where: {
                    // username: 'dan'
                },
                limit: 12
            },
            classMethods: {
                associate: function(models) {
                    User.hasMany(models.Blog
                        // , {foreignKey: 'user_id'}
                        )
                }
            }
        });
    // model.removeAttribute('id');
    // model.sync({force:true});
    // User.create({
    //     uuid: '005f9c2db58b4d0186235c0ee84d9a93',
    //     uid: 'admin',
    //     name: 'admin',
    //     email: '409747494@qq.com',
    //     tel: '15150121121',
    //     sex: 0,
    //     status: 1,
    //     password: '123456',
    //     is_admin: 1,
    //     shop_id: '',
    //     avatar_id: '234',
    //     remark: 'no-remark',
    //     rank: '123',
    // }).then(function(uc) {
    //     console.log(uc, 'this is uc')
    //     uc.createRole({role_id: 1});
    // })

    User.sync();
    return User;
}