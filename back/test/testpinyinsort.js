/**
 * Created by kevin on 17/4/29.
 */
var pinyin = require('pinyin')

var arr = [{id:1,name:'汤永城'},{id:2,name:'贾莲'},{id:1,name:'朱娇'},{id:1,name:'刘得生'},{id:1,name:'王喜凡'},{id:1,name:'王双'}]

arr.sort(function(a,b) {
    console.log(pinyin(a['name']), ' - - - - -  - -', pinyin(b['name']))
    return pinyin(a['name']) > pinyin(b['name'])
})
console.log(arr)