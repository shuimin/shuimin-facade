/**
 * Created by kevin on 16/8/10.
 */
const koa = require('koa');
const path = require('path');

console.log('in', process.env.NODE_ENV);

let app = koa();

let glob = require('./config/glob');

//注入koa
require('./config/koa')(app);
//注入路由
require('./routes')(app);

const port = process.env.PORT || 1920;

console.log('app location: ' + __filename);
console.log('started on port: ' + port);
console.log('at ' + moment().format('YYYY-MM-DD HH:mm:ss'));

var server = require('http').Server(app.callback());
var io = require('socket.io')(server)
require('./config/sockets')(io);
global.io = io;
server.listen(port);

module.exports = app;