/**
 * Created by kevin on 17/4/27.
 */
var base64 = require('./base64')

function base64Encode(uid, pwd) {
    var token = uid + ':' + pwd;
    var hash = base64.encode(token);
    return "Basic " + hash
}

module.exports = base64Encode